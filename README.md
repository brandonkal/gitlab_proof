# GitLab Proof

[Verifying my OpenPGP key: openpgp4fpr:$2a$11$Fmz7bxWPRg8Ng4gK.hyLhuEy2.BdJJo8OmyNATyHMynJmOru7Zhvy]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
